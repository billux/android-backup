# Android backup

A small script, systemd unit and udev rule to automatically backup your Android
phone on plugging in.

## Set-up

  1. Copy 95-android-backup.rules into /etc/udev/rules.d/ and the
     `ID_SERIAL_SHORT` with your Android's. To obtain the ID, run `udevadm
     monitor --environment --udev` and plug in your device.

     Once you added the rule file, you may need to reload udev with `udevadm
     control --reload`.

     If you need to debug your udev rule, you can do so with `udevadm test
     <DEVPATH>` where DEVPATH is the sysfs device path shown by `udevadm
     monitor --environment --udev`.

 2. Copy android-backup@.service into ~/.config/systemd/user/
 
 3. Enable systemd service:
    ```
    $ systemctl enable --user android-backup@.service
    ```
 4. Copy android-backup.sh into ~/.local/bin/ and android-backup.exclude into
    ~/.config/ (or tweak the paths in systemd unit and shell script).

    Some file patterns are already present in android-backup.exclude but feel
    free to add your own depending of the applications you use.

 5. On your phone, copy rsyncd.conf into /system/etc/

 6. Now plug your phone to your computer and enjoy!
