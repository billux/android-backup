#!/bin/sh

export DISPLAY=0:0
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
BACKUP_DIR=~/replicant-backup/
RSYNC_EXCLUDE=~/.config/android-backup.exclude

notify-send "Android Backup" "Starting Android backup"

(
set -e

mkdir -p "$BACKUP_DIR"
adb wait-for-device 2>&1 |(grep -Ev "^(\* daemon not running; starting now at|* daemon started successfully$)" || true)
adb root
adb wait-for-device
adb shell rsync --daemon --no-detach --config=/system/etc/rsyncd.conf &
adb forward tcp:6010 tcp:873
sleep 2
rsync -av --chmod u+rwX --delete --delete-excluded --exclude-from "$RSYNC_EXCLUDE" rsync://localhost:6010/root/ $BACKUP_DIR
adb forward --remove tcp:6010
adb shell pkill rsync
)

if [ $? -ne 0 ]; then
    notify-send "Android Backup" "Backup terminated with errors. See journalctl --user -eu android-backup for details"
else
    notify-send "Android Backup" "Backup terminated successfully"
fi
